# Arithmetic operators
a = 10
b = 3
print("a + b = ", a + b)
print("a - b = ", a - b)
print("a * b = ", a * b)
print("a / b = ", a / b)
print("a % b = ", a % b)
print("a * b = ", a * b)
print("a // b = ", a // b)

# Comparison operators
print("a > b is ", a > b)
print("a < b is ", a < b)
print("a == b is ", a == b)
print("a != b is ", a != b)
print("a >= b is ", a >= b)
print("a <= b is ", a <= b)

# Logical operators
x = True
y = False
print("x and y is ", x and y)
print("x or y is ", x or y)
print("not x is ", not x)

# Assignment operators
a += b
print("a += b is ", a)
a -= b
print("a -= b is ", a)
a *= b
print("a *= b is ", a)
a /= b
print("a /= b is ", a)
a %= b
print("a %= b is ", a)
a **= b
print("a **= b is ", a)
a //= b
print("a //= b is ", a)

# Identity operators
list1 = [1, 2, 3]
list2 = [1, 2, 3]
list3 = list1
print("list1 is list2: ", list1 is list2)
print("list1 is list3: ", list1 is list3)
print("list1 == list2: ", list1 == list2)

# Membership operators
print("1 in list1: ", 1 in list1)
print("4 not in list1: ", 4 not in list1)

# Bitwise operators
a = 10
b = 4
print("a & b = ", a & b)
print("a | b = ", a | b)
print("a ^ b = ", a ^ b)
print("~a = ", ~a)
print("a << 2 = ", a << 2)
print("a >> 2 = ", a >> 2)
