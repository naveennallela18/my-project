fn main() {
	let start = 0.1;
	let end = 1.0;
	let step = 0.1;
	let mut current = start;
	while current <=end {
		println!("{:01}",current);
		current += step;
	}
}
