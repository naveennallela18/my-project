use std::io::{self, Write};

fn main() {
    loop {
        let mut input = String::new();

        print!("Enter an expression (e.g., 2 + 3): ");
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut input).unwrap();

        let input = input.trim();

        if input == "exit" {
            break;
        }

        let parts: Vec<&str> = input.split_whitespace().collect();

        if parts.len() != 3 {
            println!("Invalid expression. Please enter an expression like '2 + 3'.");
            continue;
        }

        let num1: f64 = match parts[0].parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Invalid number: {}", parts[0]);
                continue;
            }
        };

        let operator = parts[1];

        let num2: f64 = match parts[2].parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Invalid number: {}", parts[2]);
                continue;
            }
        };

        let result = match operator {
            "+" => num1 + num2,
            "-" => num1 - num2,
            "*" => num1 * num2,
            "/" => num1 / num2,
            _ => {
                println!("Invalid operator: {}", operator);
                continue;
            }
        };

        println!("Result: {}", result);
    }
}

